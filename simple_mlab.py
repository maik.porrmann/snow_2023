#! /usr/bin/python3
import numpy as np
from mayavi import mlab
import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d
from tvtk.api import tvtk
from mayavi.scripts import mayavi2

@mayavi2.standalone
def viewMayavi(data):
  from mayavi.sources.vtk_data_source import VTKDataSource
  from mayavi.modules.outline import Outline
  from mayavi.modules.surface import Surface
  from mayavi.modules.vectors import Vectors

  mayavi.new_scene()
  # The single type one
  src = VTKDataSource(data = data)
  mayavi.add_source(src)
  mayavi.add_module(Outline())
  mayavi.add_module(Surface())
  # mayavi.add_module(Vectors())

def loadAndPlot_mayavi(filepath):
  reader =tvtk.XMLUnstructuredGridReader( file_name=filepath )
  mesh = reader.get_output()
  reader.update()
  viewMayavi(mesh)



def load_vtu(filepath):

  reader =tvtk.XMLUnstructuredGridReader( file_name=filepath )
  mesh = reader.get_output()
  reader.update()
  return mesh

def manipulateMesh(mesh):
  nodes = np.array( mesh.points )
  x = nodes[:,0]
  y = nodes[:,1]
  z = nodes[:,2]
  s = mesh.point_data.get_array('airy').to_array()
  displacement = mesh.point_data.get_array('displacement').to_array()
  assert(z.shape == displacement.shape)
  z+= displacement
  triangles = mesh.get_cells()._get_connectivity_array().to_array()
  triangles = triangles.reshape((-1,3))

  return x,y,z,triangles,s

def plot_mlab(x,y,z,triangles,s):

  s = mlab.triangular_mesh( x, y, z,triangles, scalars = s, colormap="spectral")
  mlab.show()

def plot_matplotlib(x,y,z,triangles,scalar):
  # print(triangles)
  triangles = triangles.reshape(-1)
  ax = plt.axes(projection='3d')
  cmap = plt.get_cmap('Spectral')
  collec = ax.plot_trisurf(x,y,
  #triangles,
   z, cmap=cmap)
  # collec.set_array(scalar)
  # collec.autoscale()
  plt.show()


if __name__=="__main__":
  mesh = load_vtu("data.vtu")
  viewMayavi(mesh)
  plot_mlab(*manipulateMesh(mesh))
  plot_matplotlib(*manipulateMesh(mesh))

  # loadAndPlot_mayavi("data.vtu")
