# Why use mayavi?

## build upon VTK
- uses "Traited VTK"

## mlab interface
- python access via numpy arrays
- interface similar to matplotlib
- animate view

## mayavi
- similar to paraview but simpler interface
- record interaction to script
- built-in python commandline

## What I don't know
- How to create animated views
- Can you run mlab scripts directly in mayavi?
- Whats the benefit of TVTK
- General Limitations
- All other details